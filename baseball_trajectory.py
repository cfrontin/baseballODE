#!/usr/local/bin/python

# imports
import math
import numpy as np
import numpy.linalg as nLA
import matplotlib.pyplot as plt

from scipy.integrate import solve_ivp

from weather_models import density_weather

from lift_models import lift_coeff_robinson

from drag_models import drag_coeff_robinson
from drag_models import drag_coeff_adair
from drag_models import drag_coeff_combo

from wind_models import wind_function_random

from physics_models import f_baseball
from physics_models import ground_crossing

# "an average flyball to dead center"

# exit velocity
v_exit= 140.0 # fps
launch_angle= 28.0*(np.pi/180.0) # rad from degrees

# initial spin orientation
spin_dir= 0.0*(np.pi/180.0) # rad from degrees
# initial spin rate
spin_rate= 2372.757 # RPM (insane)
# initial position (m from f)
x0= np.array([[ 0.0], \
              [ 0.0], \
              [ 4.0]])*0.3048
# initial velocity (m/s from fps)
v0= np.array([[ 0.0], \
              [ v_exit*np.sin(launch_angle)], \
              [ v_exit*np.cos(launch_angle)]])*0.3048

# rotational velocity (rad/s)
# (assuming spin is about y axis)
omega0= np.array([[-spin_rate*np.cos(spin_dir)],
                  [ 0.0],
                  [ spin_rate*np.sin(spin_dir)]])*(2*np.pi/60)
# rotational acceleration (rad/s^2)
# (assume that it's not changing...)
omegadot0= np.array([[0.0], [0.0], [0.0]])

# physical parameters
g= -9.807 # m/s^2
D_BB= 0.07379 # m
m_BB= 0.145 # kg

# frontal area of a baseball
A_BB= 0.25*np.pi*D_BB**2 # m^2

# physical variables
rho_air= 1.20578 # kg/m^3 (hand calculation less humidity correction)
temp_air= 17.22 # C
pressure_air= 101070 # Pa
humidity_air= 0.82
rho_calc= density_weather(temp_air, pressure_air, humidity_air)
print("rho_air: %f ?= rho_calc= %f" % (rho_air, rho_calc))
# from weatherunderground data from 10 june 2016 in santa ana, ca at 2000 hrs

# pack parameters
params_BB= {}
# params_BB['rho']= rho_air
params_BB['temp']= temp_air
params_BB['pressure']= pressure_air
params_BB['humidity']= humidity_air
params_BB['g']= g
params_BB['D_BB']= D_BB
params_BB['m_BB']= m_BB

# hand-ins for the IVP
y0= np.concatenate([v0, x0, omega0])
t0= 0.0
tf= 15.0
N= 100
dt= (tf - t0)/N

# for passthrough of the parameters and wind functions
def dummy(t, y):
    return f_baseball(t, y, (params_BB, dummy.model_spec,))
dummy.model_spec= {}
dummy.model_spec['wind']= wind_function_random

plt.figure(1)
plt.figure(2)
for i in range(1000):
    dummy.model_spec['wind'].random_vec= np.random.randn(3)
    sol= solve_ivp(dummy, [t0, tf], y0.squeeze().tolist(), max_step= dt,
                   events= ground_crossing, dense_output= True)
    t_lessspin= sol.t
    y_lessspin= sol.y.transpose()
    t_ground_lessspin= sol.t_events
    y_ground_lessspin= sol.sol(sol.t_events[0])
    # print(np.sqrt(y_ground_lessspin[3]**2 + y_ground_lessspin[4]**2)/0.3048)

    plt.figure(1)
    plt.plot(y_lessspin[:, 4], y_lessspin[:, 5])
    plt.title('Baseball trajectory')
    plt.xlabel('$y$')
    plt.ylabel('$z$')

    plt.figure(2)
    plt.scatter(nLA.norm(dummy.model_spec['wind'].random_vec)/0.3048,
            np.sqrt(y_ground_lessspin[3]**2 + y_ground_lessspin[4]**2)/0.3048)

plt.show()

### EOF ###
