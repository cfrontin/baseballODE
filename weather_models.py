#!/usr/local/bin/python

# imports
import math
import numpy as np

def convert_weather(T_in, p_in, H_in):
    """
    converts the commonly available units for weather data to ones useful for
    mathing with

    parameters
    ----------
    T_in : float/np.array
        the temperature of interest in degrees Fahrenheit
    p_in : float/np.array
        the pressure of interest in inches of Mercury (inHg)
    H_in : float/np.array
        the humidity of interest in percent
    """

    T_out= (T_in - 32.)*5./9. # F -> C
    p_out= 3386.389*p_in # inHg -> Pa
    H_out= H_in/100.0 # percent -> unit humidity
    return (T_out, p_out, H_out)

def density_weather(T_in, p_in= 101325, H_in= 0.0):
    """
    model for the density of a baseball flying through possibly humid air

    - uses ideal gas assumptions for air, water vapor, and mixture
    - assumes equilibriated temperature in mixture
    - uses Tetens equation for saturation pressure

    parameters
    ----------
    T_in : float
        the temperature of interest in Celsius
    p_in : float
        reported barometric pressure in Pascals
    H_in : float
        reported relative humidity on [0, 1]

    returns
    -------
    rho_out : float
        the density of the air at the conditions
    """

    assert T_in >= -30 and T_in <= 50, "physically unrealistic temperature"
    assert p_in >= 0.0, "unphysical pressure"
    assert H_in >= 0.0 and H_in <= 1.0, "unphysical humidity"

    # convert T_in from celsius to kelvin
    T_in= T_in + 273.15

    # constants
    R_dryair= 287.1 # J/(kg K)
    R_vapor= 461.5 # J/(kg K)

    # submodel specifications
    def pressure_saturation_tetens(T_K):
        T_C= T_K - 273.15
        assert T_C >= 0.0, "temperature must be positive!"
        p_sat= 610.78*np.exp(17.27*T_C/(T_C + 237.3))
        return p_sat
    def pressure_vapor(T, H):
        p_sat= pressure_saturation_tetens(T)
        p= H*p_sat
        return p
    def humidity_ratio(T, p, H):
        p_vapor= pressure_vapor(T, H)
        x= R_dryair/R_vapor*p_vapor/(p - p_vapor)
        return x

    # calculate the model
    p_vapor= pressure_vapor(T_in, H_in)
    x= humidity_ratio(T_in, p_in, H_in)

    rho_out= (p_in - p_vapor)/(R_dryair*T_in)*(1 + x)

    # print("T_in: %5.3f  p_in: %8.1f  H_in: %3.2f  p_vapor: %6.1f  x: %6.4f  rho: %8.4f"
    #         % (T_in, p_in, H_in, p_vapor, x, rho_out))

    assert rho_out > 0.0, "unphysical density"
    return rho_out
