#!/usr/local/bin/python

import numpy as np

from scipy.integrate import solve_ivp

from physics_models import f_baseball
from physics_models import ground_crossing

from lift_models import lift_coeff_robinson
from drag_models import drag_coeff_combo

import weather_models
import utils

def baseballODE(x0, v0, omega0,
        temp_air, pressure_air, humidity_air,
        N= 100,
        aero_params= None,
        t_sample= None):
    """
    input a set of initial conditions, and ambient air conditions, get a
    baseball trajectory, optionally sampled at the desired positions

    parameters
    ----------
    x0 : np.array(3x1)
        initial position in $\mathbb{R}^3$, units ft
    v0 : np.array(3x1)
        initial velocity in $\mathbb{R}^3$, units ft/s
    omega0 : np.array(3x1)
        initial spin vector in $\mathbb{R}^3$, units RPM
    temp_air : float
        air temperature in Fahrenheit
    pressure_air : float
        air pressure in inHg
    humidity_air : float
        reported percent humidity
    inferrables : dict
        inferrable model parameters
    aero_params : dict of parameters (optional)
        parameters that dictate the aerodynamical behavior of the baseball under
        the various models chosen
    t_sample : np.array (N_t x 1) (optional)
        a selections of point at which the baseball's position in-flight is
        desired

    returns
    -------
    t_out : np.array (N_t x 1)
        the times at which the equation's solution is sampled
    x_out : np.array (N_t x 3)
        the positions of the baseball (x, y, z) at the times in t_out
    v_out : np.array (N_t x 3)
        the velocities of the baseball (x, y, z) at the times in t_out
    omega_out : np.array (N_t x 3)
        the spin rates of the baseball (x, y, z) at the times in t_out
    """

    # convert types of initial condition inputs
    if type(x0) is not np.array:
        x0= np.array(x0)
    if type(v0) is not np.array:
        v0= np.array(v0)
    if type(omega0) is not np.array:
        omega0= np.array(omega0)

    # convert values of initial condition inputs to metric
    x0= utils.convert_length_ft2m(x0)
    v0= utils.convert_velocity_fps2mps(v0)
    omega0= utils.convert_spin_rpm2degs(omega0)
    omegadot0= np.zeros(omega0.shape)

    # convert units of weather data
    (temp_air, pressure_air, humidity_air)= \
            weather_models.convert_weather(temp_air, pressure_air,
                    humidity_air)

    # handle input params
    if aero_params is None:
        # physical parameters
        g= -9.807 # m/s^2
        D_BB= 0.07379 # m
        m_BB= 0.145 # kg

        # pack parameters
        aero_params= {}
        aero_params['temp']= temp_air
        aero_params['pressure']= pressure_air
        aero_params['humidity']= humidity_air
        aero_params['g']= g
        aero_params['D_BB']= D_BB
        aero_params['m_BB']= m_BB
    else:
        for key, value in utils.aero_params_default.keys():
            if key not in aero_params:
                aero_params[key]= aero_params_default[key]

    # package for IVP
    y0= np.concatenate([v0, x0, omega0])
    if t_sample is None:
        t0= 0.0
        tf= 15.0
    else:
        t0= t_sample[0]
        tf= t_sample[-1]
    dt= (tf - t0)/N
    print(t0)
    print(tf)
    print(dt)

    # passthrough of parameters
    def dummy(t, y):
      return f_baseball(t, y, (aero_params,))

    # pass to scipy integrator
    sol= solve_ivp(dummy, [t0, tf], y0.squeeze().tolist(),
            max_step= dt, t_eval= t_sample,
            events= ground_crossing, dense_output= True)

    # unpack solution vector
    t_out= sol.t
    y_out= sol.y.transpose()

    # repackage and resture original units
    v_out= utils.convert_velocity_mps2fps(y_out[:, 0:3])
    x_out= utils.convert_length_m2ft(y_out[:, 3:6])
    omega_out= utils.convert_spin_rpm2degs(y_out[:, 6:9])

    return (t_out, x_out, v_out, omega_out)

def baseballODE_inferrable(x0, v0, omega0,
        temp_air, pressure_air, humidity_air,
        inferrables,
        N= 100,
        aero_params= None,
        t_sample= None):
    """
    input a set of initial conditions, and ambient air conditions, get a
    baseball trajectory sampled at the desired positions, this time featuring
    an inferrable parameter vector for lift and drag models

    parameters
    ----------
    x0 : np.array(3x1)
      initial position in $\mathbb{R}^3$, units ft
    v0 : np.array(3x1)
      initial velocity in $\mathbb{R}^3$, units ft/s
    omega0 : np.array(3x1)
      initial spin vector in $\mathbb{R}^3$, units RPM
    temp_air : float
      air temperature in Fahrenheit
    pressure_air : float
      air pressure in inHg
    humidity_air : float
      reported percent humidity
    aero_params : dict of parameters (optional)
      parameters that dictate the aerodynamical behavior of the baseball under
      the various models chosen
    t_sample : np.array (N_t x 1) (optional)
      a selections of point at which the baseball's position in-flight is
      desired

    returns
    -------
    t_out : np.array (N_t x 1)
        the times at which the equation's solution is sampled
    x_out : np.array (N_t x 3)
        the positions of the baseball (x, y, z) at the times in t_out
    v_out : np.array (N_t x 3)
        the velocities of the baseball (x, y, z) at the times in t_out
    omega_out : np.array (N_t x 3)
        the spin rates of the baseball (x, y, z) at the times in t_out
    """

    # convert types of initial condition inputs
    if type(x0) is not np.array:
        x0= np.array(x0)
    if type(v0) is not np.array:
        v0= np.array(v0)
    if type(omega0) is not np.array:
        omega0= np.array(omega0)

    # convert values of initial condition inputs to metric
    x0= utils.convert_length_ft2m(x0)
    v0= utils.convert_velocity_fps2mps(v0)
    omega0= utils.convert_spin_rpm2degs(omega0)
    omegadot0= np.zeros(omega0.shape)

    # convert units of weather data
    (temp_air, pressure_air, humidity_air)= \
            weather_models.convert_weather(temp_air, pressure_air,
                    humidity_air)

    # handle input params
    if aero_params is None:
        # physical parameters
        g= -9.807 # m/s^2
        D_BB= 0.07379 # m
        m_BB= 0.145 # kg

        # pack parameters
        aero_params= {}
        aero_params['temp']= temp_air
        aero_params['pressure']= pressure_air
        aero_params['humidity']= humidity_air
        aero_params['g']= g
        aero_params['D_BB']= D_BB
        aero_params['m_BB']= m_BB
    else:
        for key, value in utils.aero_params_default.keys():
            if key not in aero_params:
                aero_params[key]= aero_params_default[key]

    # package for IVP
    y0= np.concatenate([v0, x0, omega0])
    if t_sample is None:
        t0= 0.0
        tf= 15.0
    else:
        t0= t_sample[0]
        tf= t_sample[-1]
    dt= (tf - t0)/N

    models= {}

    def lift_model(v, omega):
        return lift_coeff_robinson(v, omega, inferrables['CL0'],
                inferrables['lambda_L'])
    models['lift']= lift_model

    def drag_model(v, omega):
        return drag_coeff_combo(v, omega, inferrables['CD0'],
                inferrables['CDinf'], inferrables['k'], inferrables['Vcrit'],
                inferrables['m_D'])
    models['drag']= drag_model

    # passthrough of parameters
    def dummy(t, y):
      return f_baseball(t, y, (aero_params, models))

    # pass to scipy integrator
    sol= solve_ivp(dummy, [t0, tf], y0.squeeze().tolist(),
            max_step= dt, t_eval= t_sample,
            events= ground_crossing, dense_output= True)

    # unpack solution vector
    t_out= sol.t
    y_out= sol.y.transpose()

    # repackage and resture original units
    v_out= utils.convert_velocity_mps2fps(y_out[:, 0:3])
    x_out= utils.convert_length_m2ft(y_out[:, 3:6])
    omega_out= utils.convert_spin_rpm2degs(y_out[:, 6:9])

    return (t_out, x_out, v_out, omega_out)
