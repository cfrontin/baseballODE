#!/usr/local/bin/python

from numpy import pi

### UNIT CONVERSIONS

# lengths

def convert_length_ft2m(x0_ft):
    x0_metric= x0_ft*0.3048
    return x0_metric

def convert_length_m2ft(x0_metric):
    x0_ft= x0_metric/0.3048
    return x0_ft

# velocities

def convert_velocity_fps2mps(v0_fps):
    v0_metric= v0_fps*0.3048
    return v0_metric

def convert_velocity_mps2fps(v0_metric):
    v0_fps= v0_metric/0.3048
    return v0_fps

def convert_velocity_mph2fps(v0_mph):
    v0_fps= 5280./3600.*v0_mph
    return v0_fps

def convert_velocity_fps2mph(v0_fps):
    v0_mph= 3600./5280.*v0_fps
    return v0_mph

def convert_velocity_mph2mps(v0_mph):
    v0_fps= convert_velocity_mph2fps(v0_mph)
    v0_mps= convert_velocity_fps2mps(v0_fps)
    return v0_mps

def convert_velocity_mps2mph(v0_mps):
    v0_fps= convert_velocity_mps2fps(v0_mps)
    v0_mps= convert_velocity_fps2mph(v0_fps)
    return v0_mps

# spin rates

def convert_spin_rpm2degs(spin_rpm):
    spin_dps= 360.0/60.*spin_rpm
    return spin_dps

def convert_spin_degs2rpm(spin_dps):
    spin_rpm= 60.0/360.*spin_dps
    return spin_rpm

def convert_spin_rads2degs(spin_rad):
    spin_degps= 360./(2.*np.pi)*spin_rad
    return spin_degps

def convert_spin_degs2rads(spin_dps):
    spin_radps= (2.*np.pi)/360.*spin_dps
    return spin_radps

def deg2rad(deg):
    return 2.0*np.pi/360.*deg

def rad2deg(rad):
    return 360./2.0*np.pi*rad

# defaults

aero_params_default= {
    'temp': 15.0,
    'pressure': 101325.0, # standard mean sea level
    'humidity': 0.0, # dry air
    'g': -9.80665, # standard constant
    'D_BB': 0.07379, # rulebook
    'm_BB': 0.145, # rulebook
}
