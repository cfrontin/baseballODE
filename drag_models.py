
import math
import numpy as np
import numpy.linalg as nLA

def drag_coeff_robinson(v, omega, CD0= 0.300, m_D= 2.58e-4):
    """
    drag coefficient from Robinson and Robinson, 2012.

    $$C_D= C_{D_0} + m_L ||\\omega||$$

    parameters
    ----------
    v : np.array (3x1)
        velocity in $\mathbb{R}^3$
    omega : np.array (3x1)
        angular velocity in $\mathbb{R}^3$
    CL0 : float
        zero-spin lift coefficient, $C_L$, of a baseball
    lambda_L : float
        lifting function parameter

    returns
    -------
    CD : float
        experienced drag coefficient under the model
    """
    CD= CD0 + m_D*nLA.norm(omega)
    return CD

def drag_coeff_adair(v, omega, CD0= 0.6, CDinf= 0.2, k= 1./125., Vcrit= 24.384):
    """
    drag coefficient from Adair, 2002.

    $$C_D= C_{D_0} + (C_{D_\infty} - C_{D_0})/(1 + \exp(-k (||v||
            - V_\mathrm{crit})))$$

    parameters
    ----------
    v : np.array (3x1)
        velocity in $\mathbb{R}^3$
    omega : np.array (3x1)
        angular velocity in $\mathbb{R}^3$
    CD0 : float
        zero-Re asymptotic $C_D$ of a baseball
    CDinf : float
        infinite Re asymptotic $C_D$ of a baseball
    k : float
        total width of band of velocities over which laminar-turbulent transition
        occurs, centered at Vcrit
    Vcrit : float
        critical velocity of midpoint of transition band in velocity space

    returns
    -------
    CD : float
        experienced drag coefficient under the model
    """
    CD= CD0 + (CDinf - CD0)/(1.0 + np.exp(-k*(nLA.norm(v) - Vcrit)))
    return CD

def drag_coeff_combo(v, omega, CD0= 0.6, CDinf= 0.2, k= 1./125.,
                     Vcrit= 24.384, m_D= 2.58e-4):
    """
    drag coefficient from Adair 2002 with linear spin correction via
    Robinson & Robinson (2012).

    parameters
    ----------
    v : np.array (3x1)
        velocity in $\mathbb{R}^3$
    omega : np.array (3x1)
        angular velocity in $\mathbb{R}^3$
    CD0 : float
        zero-Re asymptotic $C_D$ of a baseball
    CDinf : float
        infinite Re asymptotic $C_D$ of a baseball
    k : float
        total width of band of velocities over which laminar-turbulent transition
        occurs, centered at Vcrit
    Vcrit : float
        critical velocity of midpoint of transition band in velocity space
    m_D : float
        slope of spin-based velocity decay

    returns
    -------
    CD : float
        experienced drag coefficient under the model
    """
    CDpre= drag_coeff_adair(v, omega, CD0= CD0, CDinf= CDinf, k= k, Vcrit= Vcrit)
    CD= drag_coeff_robinson(v, omega, CD0= CDpre, m_D= m_D)
    return CD
