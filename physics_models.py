
# imports
import math
import numpy as np
import numpy.linalg as nLA

from weather_models import density_weather

from lift_models import lift_coeff_robinson

from drag_models import drag_coeff_robinson
from drag_models import drag_coeff_adair
from drag_models import drag_coeff_combo

def f_baseball(t, y, argin= None):
    """
    ODE that governs the flight of a baseball

    $$
    \ddot{\mathbf{x}}= \frac{\mathbf{F}_\mathrm{M}}{m_\mathrm{BB}}
            + \frac{\mathbf{F}_\mathrm{D}}{m_\mathrm{BB}} + \mathbf{g}
    $$
    where:
    $$
    \mathbf{F}_\mathrm{D}= - \frac{1}{2} \rho A_\mathrm{BB} C_D
            ||(\mathbf{V} - \mathbf{v}_w)|| (\mathbf{V} - \mathbf{v}_w)
    $$
    and
    $$
    \mathbf{F}_\mathrm{M}= \frac{1}{2} \rho A_\mathrm{BB} C_L
            \frac{||(\mathbf{V} - \mathbf{v}_w)||}{||\boldsymbol{\omega}||}
            (\boldsymbol{\omega} \times (\mathbf{V} - \mathbf{v}_w))
    $$

    parameters
    ----------
    t : float
        the time (i think it's unused...)
    y : list(float)
        the state vector ($y= [ \dot{\mathbf{x}}^\mathrm{T},
        \mathbf{x}^\mathrm{T}, \boldsymbol \omega^\mathrm{T} ]$)
    argin : list
        arguments for passing in parameters!

    returns
    -------
    ydot : list(float)
        the temporal derivative of the state vector ($\dot{y}$)

    """

    # parameters
    params= argin[0]
    if 'rho' in params.keys():
        rho= params['rho']
    else:
        rho= density_weather(params['temp'], params['pressure'],
                params['humidity'])
    g= params['g']
    m_BB= params['m_BB']
    D_BB= params['D_BB']

    # frontal area of a baseball from the diameter handed in
    A_BB= 0.25*np.pi*D_BB**2 # m^2

    models= None

    if len(argin) > 1:
        models= argin[1]

    if models is None: # default configuration
        models= {}
        models['lift']= lift_coeff_robinson
        models['drag']= drag_coeff_combo
        models['wind']= None
        models['spin']= None
    else: # anything not specified in the inputs, use default
        if 'lift' not in models.keys():
            models['lift']= lift_coeff_robinson
        if 'drag' not in models.keys():
            models['drag']= drag_coeff_combo
        if 'wind' not in models.keys():
            models['wind']= None
        if 'spin' not in models.keys():
            models['spin']= None

    ydot= np.zeros(y.shape)

    # unpack variables
    v= np.array(y[0:3])
    x= np.array(y[3:6])
    omega= np.array(y[6:9])

    # set in the wind model if it exists
    if models['wind'] is not None:
        # wind function parameters should be built in
        v_wind= models['wind'](t, y)
    else:
        # otherwise, use zero wind vector
        v_wind= np.array([0.0, 0.0, 0.0])

    # get the coefficients
    C_D= models['drag'](v - v_wind, omega)
    C_L= models['lift'](v - v_wind, omega)

    # get the acceleration: gravity only
    a_G= [0.0, 0.0, g]

    # drag force
    F_D= -0.5*rho*A_BB*C_D*nLA.norm(v - v_wind)*(v - v_wind)

    # magnus lift
    if nLA.norm(omega) > 1e-8:
        F_M= 0.5*rho*A_BB*C_L*nLA.norm(v - v_wind)/nLA.norm(omega) \
                *np.cross(omega, (v - v_wind))
    else:
        F_M= np.zeros(omega.shape)

    # d2x/dt2
    ydot[0]= 1/m_BB*(F_D[0] + F_M[0]) + a_G[0]
    ydot[1]= 1/m_BB*(F_D[1] + F_M[1]) + a_G[1]
    ydot[2]= 1/m_BB*(F_D[2] + F_M[2]) + a_G[2]

    ydot[3]= y[0]
    ydot[4]= y[1]
    ydot[5]= y[2]

    # if spin model is specified: use it
    if models['spin'] is not None:
        raise ImplementationError("no spin model implemented yet.")
    elif models['spin'] == "decay":
        lambda_spin= 2.4 # Hz
        ydot[6:9]= -lambda_spin*omega
    else:
        ydot[6:9]= 0.0

    return ydot.tolist()

def ground_crossing(t, y):
    """
    an event function for when the ball comes close to the ground

    parameters
    ----------
    t : float
        time
    y : list(float)
        the state vector ($y= [ \dot{\mathbf{x}}^\mathrm{T},
        \mathbf{x}^\mathrm{T}, \boldsymbol \omega^\mathrm{T} ]$)

    returns
    -------
    z : float
        the vertical coordinate value from y
    """
    return y[5]
ground_crossing.terminal= True
ground_crossing.direction= -1
