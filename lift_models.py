
import math
import numpy as np
import numpy.linalg as nLA

# from matplotlib import pyplot as plt

def lift_coeff_robinson(v, omega, CL0= 0.319, lambda_L= 2.48e-3):
    """
    lift coefficient from Robinson and Robinson, 2012.

    $$C_L= C_{L_0} (1 - \exp(-\lambda_L ||\\omega||))$$

    parameters
    ----------
    v : np.array (3x1)
        velocity in $\mathbb{R}^3$
    omega : np.array (3x1)
        angular velocity in $\mathbb{R}^3$
    CL0 : float
        zero-spin lift coefficient, $C_L$, of a baseball
    lambda_L : float
        lifting function parameter

    returns
    -------
    CL : float
        experienced lift coefficient under the model
    """
    CL= CL0*(1.0 - np.exp(-lambda_L*nLA.norm(omega)))
    return CL

# def test():
#     omega_vec= np.linspace(0, 1000, 101)
#     CL_vec= np.zeros(omega_vec.shape)
#     for i in range(len(omega_vec)):
#         CL_vec[i]= lift_coeff_robinson(30, omega_vec[i])
#     plt.plot(omega_vec, CL_vec)
#     plt.show()
#
# test()
